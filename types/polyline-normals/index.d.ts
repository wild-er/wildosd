declare module "polyline-normals" {
	function foo (path: Array<Array<number>>, closed: boolean): Array<[Array<number>,number]>;
	export = foo;
}
