import OpenSeadragon, { Point } from 'openseadragon';
import getNormals from 'polyline-normals';
import { WildCanvasOverlay } from './WildCanvasOverlay';
import { WildOsdDragmag } from './WildOsdDragmag';

const HALF_PI: number = Math.PI / 2.0;
const THIRD_PI: number = Math.PI / 3.0;
const FOURTH_PI: number = Math.PI / 4.0;
const SIXTH_PI: number = Math.PI / 6.0;

const defaultShaderType = ['VERTEX_SHADER', 'FRAGMENT_SHADER'];

const vertexShader2d = `
  attribute vec2 a_position;
  attribute vec2 a_shape;
	uniform mat3 local_scale_matrix;
	uniform mat3 u_matrix;
	uniform float zoom_factor;

	void main() {
		//Local scale
		vec2 s = (zoom_factor * local_scale_matrix * vec3(a_shape, 1)).xy;
		//Translate
  	vec2 p = s + a_position;
  	// Multiply the position by the matrix.
  	gl_Position = vec4((u_matrix * vec3(p, 1)).xy, 0, 1);
	}
  `;

const vertexShader2dLines = `
	attribute vec2 a_position;
	attribute vec2 normal;
	attribute float miter;
	uniform mat3 u_matrix;
	uniform float thickness;

	void main() {
		//push the point along its normal by half thickness
		vec2 p = a_position.xy + vec2(normal * thickness/2.0 * miter);
		// Multiply the position by the matrix.
		gl_Position = vec4((u_matrix * vec3(p, 1)).xy, 0, 1);
	}
	`;

const fragmentShader2d = `
	precision mediump float;
	uniform vec4 u_color;

	void main() {
	   gl_FragColor = u_color;
	}
  `;

const m3 = {
  projection(width: number, height: number) {
    // Note: This matrix flips the Y axis so that 0 is at the top.
    return [2 / width, 0, 0, 0, -2 / height, 0, -1, 1, 1];
  },

  identity: function () {
    return [1, 0, 0, 0, 1, 0, 0, 0, 1];
  },

  translation: function (tx: number, ty: number) {
    return [1, 0, 0, 0, 1, 0, tx, ty, 1];
  },

  rotation: function (angleInRadians: number) {
    const c = Math.cos(angleInRadians);
    const s = Math.sin(angleInRadians);
    return [c, -s, 0, s, c, 0, 0, 0, 1];
  },

  scaling: function (sx: number, sy: number) {
    return [sx, 0, 0, 0, sy, 0, 0, 0, 1];
  },

  multiply: function (a: Array<number>, b: Array<number>) {
    const a00 = a[0 * 3 + 0];
    const a01 = a[0 * 3 + 1];
    const a02 = a[0 * 3 + 2];
    const a10 = a[1 * 3 + 0];
    const a11 = a[1 * 3 + 1];
    const a12 = a[1 * 3 + 2];
    const a20 = a[2 * 3 + 0];
    const a21 = a[2 * 3 + 1];
    const a22 = a[2 * 3 + 2];
    const b00 = b[0 * 3 + 0];
    const b01 = b[0 * 3 + 1];
    const b02 = b[0 * 3 + 2];
    const b10 = b[1 * 3 + 0];
    const b11 = b[1 * 3 + 1];
    const b12 = b[1 * 3 + 2];
    const b20 = b[2 * 3 + 0];
    const b21 = b[2 * 3 + 1];
    const b22 = b[2 * 3 + 2];
    return [
      b00 * a00 + b01 * a10 + b02 * a20,
      b00 * a01 + b01 * a11 + b02 * a21,
      b00 * a02 + b01 * a12 + b02 * a22,
      b10 * a00 + b11 * a10 + b12 * a20,
      b10 * a01 + b11 * a11 + b12 * a21,
      b10 * a02 + b11 * a12 + b12 * a22,
      b20 * a00 + b21 * a10 + b22 * a20,
      b20 * a01 + b21 * a11 + b22 * a21,
      b20 * a02 + b21 * a12 + b22 * a22,
    ];
  },
};

export class WebGlOverlay extends WildCanvasOverlay {
  private gl: WebGL2RenderingContext | null;
  private programTriStrips: WebGLProgram | null = null;
  private programLines: WebGLProgram | null = null;
  private triStripShapeLocation: number = 0;
  private triStripPositionLocation: number = 0;
  private linesPositionLocation: number = 0;
  private linesNormalLocation: number = 0;
  private linesMiterLocation: number = 0;
  private tristripShapeBuffer: WebGLBuffer | null = null;
  private tristripPositionBuffer: WebGLBuffer | null = null;

  private tristripLength: Array<number>;
  private linesPositionBuffer: WebGLBuffer | null = null;
  private linesNormalBuffer: WebGLBuffer | null = null;
  private linesMiterBuffer: WebGLBuffer | null = null;
  private linesIndexBuffer: WebGLBuffer | null = null;
  private linesSizes: Array<number>;
  private triStripColorLocation: WebGLUniformLocation | null = null;
  private linesColorLocation: WebGLUniformLocation | null = null;
  private lineThicknessLocation: WebGLUniformLocation | null = null;
  private triStripMatrixLocation: WebGLUniformLocation | null = null;
  private triStripZoomLocation: WebGLUniformLocation | null = null;
  private triStripScaleMatrixLocation: WebGLUniformLocation | null = null;
  private linesMatrixLocation: WebGLUniformLocation | null = null;
  private translation: Array<number> = [0, 0];
  private angleInRadians: number = 0;
  private scale: Array<number> = [0, 0];
  private drawTriStrip = false;
  private drawLine = false;
  private colorsRGB: Array<Array<number>>;
  private triStripcolorsIndexes: Array<number>;
  private linesColorIndexes: Array<number>;
  public maxMiterSize = 10;
  public lineThickness = 1000;
  private polygonFactorMinZoom = 6;
  private polygonFactorMaxZoom = 3;
  private viewportMinZoom: number;
  private viewportMaxZoom: number;
  private zoomSpeed = 1000;
  public drawStrokes = true;
  public strokeColor: Array<number> = [0.0, 0.0, 0, 1.0];
  public strokeFactor = 1.2;
  private resizeRetryFactor = 0.95;
  private offScreenBufferSizes: Map<OpenSeadragon.Viewer, [number, number]>;

  constructor(
    viewer: OpenSeadragon.Viewer,
    options: OpenSeadragon.Options,
    root: (Node & ParentNode) | null,
    colorsRGB: Array<Array<number>>,
  ) {
    super(viewer, options);

    this.colorsRGB = colorsRGB;
    this.tristripLength = new Array<number>();
    this.triStripcolorsIndexes = new Array<number>();
    this.linesColorIndexes = new Array<number>();
    this.linesSizes = new Array<number>();

    this.offScreenCanvas = document.createElement('canvas');
    this.offScreenBufferSizes = new Map<OpenSeadragon.Viewer, [number, number]>();

    this.viewer.addHandler('update-viewport', () => {
      this.resize(this.viewer);
      this.updateCanvas(this.viewer);
    });
    this.viewer.addHandler('open', () => {
      console.log('OPEN');
      this.resize(this.viewer);
      this.updateCanvas(this.viewer);
    });

    this.viewportMinZoom = viewer.viewport.getMinZoom();
    this.viewportMaxZoom = viewer.viewport.getMaxZoom();

    // WEBGL INIT

    this.gl = this.getOffScreenContext();
    if (!this.gl) {
      console.error('Cannot get WebGL2RenderingContext');
      return;
    }
    // setup GLSL program
    this.programTriStrips = this.createProgramFromSources(
      [vertexShader2d, fragmentShader2d],
      [],
      [],
      [this.gl.VERTEX_SHADER, this.gl.FRAGMENT_SHADER],
    );

    // Load 2D lines vertex shader
    this.programLines = this.createProgramFromSources(
      [vertexShader2dLines, fragmentShader2d],
      [],
      [],
      [this.gl.VERTEX_SHADER, this.gl.FRAGMENT_SHADER],
    );

    if (!this.programTriStrips || !this.programLines) return;

    // look up where the vertex data needs to go.
    this.triStripShapeLocation = this.gl.getAttribLocation(this.programTriStrips, 'a_shape');
    this.triStripPositionLocation = this.gl.getAttribLocation(this.programTriStrips, 'a_position');
    this.linesPositionLocation = this.gl.getAttribLocation(this.programLines, 'a_position');

    // Lookup lines
    this.linesNormalLocation = this.gl.getAttribLocation(this.programLines, 'normal');
    this.linesMiterLocation = this.gl.getAttribLocation(this.programLines, 'miter');

    // lookup uniforms
    this.triStripColorLocation = this.gl.getUniformLocation(this.programTriStrips, 'u_color');
    this.linesColorLocation = this.gl.getUniformLocation(this.programLines, 'u_color');
    this.triStripScaleMatrixLocation = this.gl.getUniformLocation(this.programTriStrips, 'local_scale_matrix');
    this.triStripMatrixLocation = this.gl.getUniformLocation(this.programTriStrips, 'u_matrix');
    this.linesMatrixLocation = this.gl.getUniformLocation(this.programLines, 'u_matrix');
    this.lineThicknessLocation = this.gl.getUniformLocation(this.programLines, 'thickness');
    this.triStripZoomLocation = this.gl.getUniformLocation(this.programTriStrips, 'zoom_factor');

    // Create a buffer to put tristrip shapes and position in
    this.tristripShapeBuffer = this.gl.createBuffer();
    this.tristripPositionBuffer = this.gl.createBuffer();

    // Create buffers for lines
    this.linesPositionBuffer = this.gl.createBuffer();
    this.linesNormalBuffer = this.gl.createBuffer();
    this.linesMiterBuffer = this.gl.createBuffer();
    this.linesIndexBuffer = this.gl.createBuffer();
  }

  // Draw the scene.
  drawScene(zoom: number) {
    if (!this.gl) return;

    // Tell WebGL how to convert from clip space to pixels
    this.gl.viewport(0, 0, this.gl.drawingBufferWidth, this.gl.drawingBufferHeight);

    //this.gl.clearColor(1.0, 0, 0, 0.5);

    // Clear the canvas.
    this.gl.clear(this.gl.COLOR_BUFFER_BIT);

    // Compute the matrices
    const projectionMatrix = m3.projection(this.containerWidth, this.containerHeight);

    const translationMatrix = m3.translation(this.translation[0], this.translation[1]);
    const rotationMatrix = m3.rotation(this.angleInRadians);

    const scaleMatrix = m3.scaling(this.scale[0], this.scale[1]);

    // Multiply the matrices.
    let matrix = m3.multiply(projectionMatrix, translationMatrix);
    matrix = m3.multiply(matrix, rotationMatrix);
    matrix = m3.multiply(matrix, scaleMatrix);

    const localScaleMatrix = m3.scaling(zoom / this.scale[0], zoom / this.scale[1]);

    // Call the draw tristrips callback.
    if (this.drawTriStrip) {
      // Bind the shape buffer.
      this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.tristripShapeBuffer);

      // Turn on the attribute
      this.gl.enableVertexAttribArray(this.triStripShapeLocation);

      // Tell the attribute how to get data out of positionBuffer (ARRAY_BUFFER)
      // 2 components per iteration
      // the data is 32bit floats
      // don't normalize the data
      // 0 = move forward size * sizeof(type) each iteration to get the next position
      // start at the beginning of the buffer
      this.gl.vertexAttribPointer(this.triStripShapeLocation, 2, this.gl.FLOAT, false, 0, 0);

      // Bind the shape buffer.
      this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.tristripPositionBuffer);
      this.gl.enableVertexAttribArray(this.triStripPositionLocation);
      this.gl.vertexAttribPointer(this.triStripPositionLocation, 2, this.gl.FLOAT, false, 0, 0);

      // Tell it to use our program (pair of shaders)
      this.gl.useProgram(this.programTriStrips);
      // Set the transformation matrix.
      this.gl.uniformMatrix3fv(this.triStripMatrixLocation, false, matrix);
      // Set the scale matrix.
      this.gl.uniformMatrix3fv(this.triStripScaleMatrixLocation, false, localScaleMatrix);

      this.drawConvexPolygons();
      this.gl.disableVertexAttribArray(this.triStripShapeLocation);
      this.gl.disableVertexAttribArray(this.triStripPositionLocation);
    }

    // Call the draw lines callback.
    if (this.drawLine) {
      // Bind the line position buffer.
      this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.linesPositionBuffer);
      this.gl.enableVertexAttribArray(this.linesPositionLocation);
      this.gl.vertexAttribPointer(this.linesPositionLocation, 2, this.gl.FLOAT, false, 0, 0);

      // Bind the line normal buffer.
      this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.linesNormalBuffer);
      this.gl.enableVertexAttribArray(this.linesNormalLocation);
      this.gl.vertexAttribPointer(this.linesNormalLocation, 2, this.gl.FLOAT, false, 0, 0);

      // Bind the line miter buffer.
      this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.linesMiterBuffer);
      this.gl.enableVertexAttribArray(this.linesMiterLocation);
      this.gl.vertexAttribPointer(this.linesMiterLocation, 1, this.gl.FLOAT, false, 0, 0);

      this.gl.bindBuffer(this.gl.ELEMENT_ARRAY_BUFFER, this.linesIndexBuffer);

      // Switch program
      this.gl.useProgram(this.programLines);

      // Set the matrix.
      this.gl.uniformMatrix3fv(this.linesMatrixLocation, false, matrix);

      this.drawLines();

      this.gl.disableVertexAttribArray(this.linesPositionLocation);
      this.gl.disableVertexAttribArray(this.linesNormalLocation);
      this.gl.disableVertexAttribArray(this.linesMiterLocation);
    }
  }

  private tryResizeOffscreenCanvas(viewer: OpenSeadragon.Viewer, width: number, height: number) {
    let previousSize = this.offScreenBufferSizes.get(viewer);
    if (previousSize) {
      // @ts-ignore
      this.offScreenCanvas.width = previousSize[0];
      // @ts-ignore
      this.offScreenCanvas.height = previousSize[1];
    } else {
      // @ts-ignore
      this.offScreenCanvas.height = height;
      // @ts-ignore
      this.offScreenCanvas.width = width;

      if (
        this.gl?.drawingBufferHeight != this.offScreenCanvas.height ||
        this.gl?.drawingBufferWidth != this.offScreenCanvas.width
      ) {
        console.log('Offscreen canvas still too big ' + this.offScreenCanvas.width + 'x' + this.offScreenCanvas.height);
        this.tryResizeOffscreenCanvas(viewer, this.resizeRetryFactor * width, this.resizeRetryFactor * height);
      } else {
        console.log('Offscreen canvas final size ' + this.offScreenCanvas.width + 'x' + this.offScreenCanvas.height);
        this.offScreenBufferSizes.set(viewer, [this.offScreenCanvas.width, this.offScreenCanvas.height]);
      }
    }
  }

  public resize(viewer: OpenSeadragon.Viewer) {
    let backingScale = 1;
    if (typeof window !== 'undefined' && 'devicePixelRatio' in window) {
      backingScale = window.devicePixelRatio;
    }
    const backingScaleUpdated = this.backingScale !== backingScale;
    this.backingScale = backingScale;

    if (this.containerWidth !== viewer.container.clientWidth || backingScaleUpdated) {
      this.containerWidth = viewer.container.clientWidth;
    }
    if (this.containerHeight !== viewer.container.clientHeight || backingScaleUpdated) {
      this.containerHeight = viewer.container.clientHeight;
    }

    this.tryResizeOffscreenCanvas(viewer, this.containerWidth, this.containerHeight);
  }

  public setLines(points: Array<Array<Array<number>>>, colorsIndexes: Array<number>) {
    this.linesColorIndexes = colorsIndexes;
    let lineTotalSize = 0;
    points.forEach((value) => {
      lineTotalSize += value.length;
    });

    // Reset trails sizes
    this.linesSizes = new Array<number>(points.length);

    const glPathPositionArray = new Float32Array(lineTotalSize * 4); // 2 vertices per line point
    const glPathNormalArray = new Float32Array(lineTotalSize * 4);
    const glPathMiterArray = new Float32Array(lineTotalSize * 2);
    const glPathIndexArray = new Uint16Array(lineTotalSize * 6);

    let trailIndex = 0;
    let trailIndex2 = 0;
    let sizeIndex = 0;
    let indexBufferIndex = 0;
    points.forEach((positions, key) => {
      const normalsAndMiter = getNormals(positions, false);
      // The data contains a normal, [nx, ny] and the length of the miter (default to 1.0 where no join occurs).
      // [
      // 	[ [nx, ny], miterLength ],
      // 	[ [nx, ny], miterLength ]
      // ]

      const normals = new Array<Array<number>>();
      const miters = new Array<number>();

      for (let i = 0; i < normalsAndMiter.length; i++) {
        //Cap mitter because it could be Infinite if angle is very small
        if (normalsAndMiter[i][1] >= this.maxMiterSize || normalsAndMiter[i][1] <= -this.maxMiterSize) {
          normalsAndMiter[i][1] = 1;
        }

        normals.push([normalsAndMiter[i][0][0], normalsAndMiter[i][0][1]]);
        miters.push(normalsAndMiter[i][1]);
      }

      if (normals && miters) {
        for (let i = 0; i < positions.length; i++) {
          // vertex attributes (positions, normals) need to be duplicated
          // the only difference is that one has a negative miter length
          glPathPositionArray[trailIndex2] = positions[i][0];
          glPathPositionArray[trailIndex2 + 1] = positions[i][1];
          glPathPositionArray[trailIndex2 + 2] = positions[i][0];
          glPathPositionArray[trailIndex2 + 3] = positions[i][1];
          glPathNormalArray[trailIndex2] = normals[i][0];
          glPathNormalArray[trailIndex2 + 1] = normals[i][1];
          glPathNormalArray[trailIndex2 + 2] = normals[i][0];
          glPathNormalArray[trailIndex2 + 3] = normals[i][1];
          glPathMiterArray[trailIndex] = -miters[i];
          glPathMiterArray[trailIndex + 1] = miters[i];

          glPathIndexArray[indexBufferIndex++] = trailIndex + 0;
          glPathIndexArray[indexBufferIndex++] = trailIndex + 1;
          glPathIndexArray[indexBufferIndex++] = trailIndex + 2;
          glPathIndexArray[indexBufferIndex++] = trailIndex + 2;
          glPathIndexArray[indexBufferIndex++] = trailIndex + 1;
          glPathIndexArray[indexBufferIndex++] = trailIndex + 3;

          trailIndex += 2;
          trailIndex2 += 4;
        }
        this.linesSizes[sizeIndex++] = positions.length;
      } else {
        console.error('Missing normal or miter for ' + key);
      }
    });

    this.setLinesBuffer(glPathPositionArray, glPathNormalArray, glPathMiterArray, glPathIndexArray);
  }

  private drawLines() {
    // @ts-ignore
    this.gl.uniform1f(this.lineThicknessLocation, this.lineThickness);
    let triangleIndex = 0;
    for (let i = 0; i < this.linesSizes.length; i++) {
      // @ts-ignore
      this.gl.uniform4fv(this.linesColorLocation, [
        this.colorsRGB[this.linesColorIndexes[i]][0],
        this.colorsRGB[this.linesColorIndexes[i]][1],
        this.colorsRGB[this.linesColorIndexes[i]][2],
        1.0,
      ]);
      const currentPathSize = this.linesSizes[i];
      // @ts-ignore
      this.gl.drawElements(this.gl.TRIANGLES, (currentPathSize - 1) * 6, this.gl.UNSIGNED_SHORT, triangleIndex); // 3 vertices line × 2 coordinates = 6 elements.
      triangleIndex += currentPathSize * 6 * 2; // unsigned short 2 bytes * 6 vertices index (2 triangles)
    }
  }

  public setConvexPolygons(
    shapes: Array<Array<Array<number>>>,
    positions: Array<Array<number>>,
    colorsIndexes: Array<number>,
  ) {
    let size = 0;
    this.tristripLength = new Array<number>(shapes.length);
    this.triStripcolorsIndexes = colorsIndexes;

    shapes.forEach((path, index) => {
      if (path.length < 3) {
        console.error('Polygon size < 3');
      }
      size += path.length * 2;
      this.tristripLength[index] = path.length * 2; //*2: x+y
    });

    const tristripVertexArray: Float32Array = new Float32Array(size);
    const tristripVertexPositions: Float32Array = new Float32Array(size);

    let index = 0;
    let positionIndex = 0;
    shapes.forEach((path) => {
      let i = 1;
      let j = path.length - 1;
      let left = false;
      tristripVertexPositions[index] = positions[positionIndex][0];
      tristripVertexArray[index++] = path[0][0];
      tristripVertexPositions[index] = positions[positionIndex][1];
      tristripVertexArray[index++] = path[0][1];

      while (i <= j) {
        if (left) {
          tristripVertexPositions[index] = positions[positionIndex][0];
          tristripVertexArray[index++] = path[i][0];
          tristripVertexPositions[index] = positions[positionIndex][1];
          tristripVertexArray[index++] = path[i][1];
          i++;
        } else {
          tristripVertexPositions[index] = positions[positionIndex][0];
          tristripVertexArray[index++] = path[j][0];
          tristripVertexPositions[index] = positions[positionIndex][1];
          tristripVertexArray[index++] = path[j][1];
          j--;
        }
        left = !left;
      }
      positionIndex++;
    });
    this.setTriStripBuffer(tristripVertexArray, tristripVertexPositions);
  }

  private drawConvexPolygons() {
    let tristripIndex = 0;

    this.tristripLength.forEach((tristripLength: number, index: number) => {
      const vertexNumber = tristripLength / 2;

      if (this.drawStrokes) {
        // set the color for the stroke
        // @ts-ignore
        this.gl.uniform4fv(this.triStripColorLocation, this.strokeColor);

        // @ts-ignore
        this.gl.uniform1f(this.triStripZoomLocation, this.strokeFactor);

        //Draw the scaled version first
        // @ts-ignore
        this.gl.drawArrays(this.gl.TRIANGLE_STRIP, tristripIndex, vertexNumber);
      }

      //Reset scale factor
      // @ts-ignore
      this.gl.uniform1f(this.triStripZoomLocation, 1.0);

      //Set the color of the polygon
      // @ts-ignore
      this.gl.uniform4fv(this.triStripColorLocation, [
        this.colorsRGB[this.triStripcolorsIndexes[index]][0],
        this.colorsRGB[this.triStripcolorsIndexes[index]][1],
        this.colorsRGB[this.triStripcolorsIndexes[index]][2],
        1.0,
      ]);
      // Draw the polygon
      // @ts-ignore
      this.gl.drawArrays(this.gl.TRIANGLE_STRIP, tristripIndex, vertexNumber);
      //Go to the next tristrip
      tristripIndex += vertexNumber;
    });
  }

  // Fill the tristrip buffer
  private setTriStripBuffer(shapeBuffer: ArrayBufferView, positionBuffer: ArrayBufferView) {
    if (shapeBuffer !== undefined && positionBuffer !== undefined && this.gl !== null) {
      // Bind and feed
      this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.tristripShapeBuffer);
      this.gl.bufferData(this.gl.ARRAY_BUFFER, shapeBuffer, this.gl.STATIC_DRAW);
      // Bind and feed
      this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.tristripPositionBuffer);
      this.gl.bufferData(this.gl.ARRAY_BUFFER, positionBuffer, this.gl.STATIC_DRAW);
      this.drawTriStrip = true;
    }
  }

  // Fill the lines buffer.
  private setLinesBuffer(
    positionBuffer: ArrayBufferView,
    normalBuffer: ArrayBufferView,
    miterBuffer: ArrayBufferView,
    indexBuffer: ArrayBufferView,
  ) {
    if (
      positionBuffer !== undefined &&
      normalBuffer !== undefined &&
      miterBuffer !== undefined &&
      indexBuffer !== undefined &&
      this.gl !== null
    ) {
      this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.linesPositionBuffer);
      this.gl.bufferData(this.gl.ARRAY_BUFFER, positionBuffer, this.gl.STATIC_DRAW);
      this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.linesNormalBuffer);
      this.gl.bufferData(this.gl.ARRAY_BUFFER, normalBuffer, this.gl.STATIC_DRAW);
      this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.linesMiterBuffer);
      this.gl.bufferData(this.gl.ARRAY_BUFFER, miterBuffer, this.gl.STATIC_DRAW);
      this.gl.bindBuffer(this.gl.ELEMENT_ARRAY_BUFFER, this.linesIndexBuffer);
      this.gl.bufferData(this.gl.ELEMENT_ARRAY_BUFFER, indexBuffer, this.gl.STATIC_DRAW);
      if (positionBuffer.byteLength > 0) this.drawLine = true;
    }
  }

  private loadShader(shaderSource: string, shaderType: number) {
    if (!this.gl) return null;
    // Create the shader object
    const shader = this.gl.createShader(shaderType);

    if (!shader) return null;

    // Load the shader source
    this.gl.shaderSource(shader, shaderSource);

    // Compile the shader
    this.gl.compileShader(shader);

    // Check the compile status
    const compiled = this.gl.getShaderParameter(shader, this.gl.COMPILE_STATUS);
    if (!compiled) {
      // Something went wrong during compilation; get the error
      const lastError = this.gl.getShaderInfoLog(shader);
      console.error(
        "*** Error compiling shader '" +
          shader +
          "':" +
          lastError +
          `\n` +
          shaderSource
            .split('\n')
            .map((l, i) => `${i + 1}: ${l}`)
            .join('\n'),
      );
      this.gl.deleteShader(shader);
      return null;
    }

    return shader;
  }

  private createProgram(shaders: Array<WebGLShader>, optAttribs: Array<string>, optLocations: Array<number>) {
    if (!this.gl) return null;
    const program = this.gl.createProgram();
    if (!program) return null;
    shaders.forEach((shader) => {
      // @ts-ignore
      this.gl.attachShader(program, shader);
    });
    if (optAttribs) {
      optAttribs.forEach((attrib, ndx) => {
        // @ts-ignore
        this.gl.bindAttribLocation(program, optLocations ? optLocations[ndx] : ndx, attrib);
      });
    }
    this.gl.linkProgram(program);

    // Check the link status
    const linked = this.gl.getProgramParameter(program, this.gl.LINK_STATUS);
    if (!linked) {
      // something went wrong with the link
      const lastError = this.gl.getProgramInfoLog(program);
      console.error('Error in program linking:' + lastError);
      this.gl.deleteProgram(program);
      return null;
    }
    return program;
  }

  public static circleToPath(x: number, y: number, radius: number, segmentsNumber: number) {
    const angleShift = (Math.PI * 2) / segmentsNumber;
    let angle = 0;
    const points = new Array<Array<number>>(segmentsNumber);
    for (let i = 0; i < segmentsNumber; i++) {
      points[i] = [x + radius * Math.cos(angle), y + radius * Math.sin(angle)];
      angle += angleShift;
    }
    return points;
  }

  public setPolygonZoomFactor(polygonFactorMinZoom: number, polygonFactorMaxZoom: number, zoomSpeed = 1000) {
    if (polygonFactorMinZoom < polygonFactorMaxZoom) {
      this.polygonFactorMinZoom = polygonFactorMaxZoom;
      console.error('setPolygonZoomFactor polygonFactorMinZoom must be greater or equal to polygonFactorMaxZoom');
    } else this.polygonFactorMinZoom = polygonFactorMinZoom;

    this.polygonFactorMaxZoom = polygonFactorMaxZoom;

    this.zoomSpeed = zoomSpeed;
  }

  private createProgramFromSources(
    shaderSources: Array<string>,
    optAttribs: Array<string>,
    optLocations: Array<number>,
    types: Array<number>,
  ) {
    if (!this.gl) return null;
    const shaders = new Array<WebGLShader>();
    for (let i = 0; i < shaderSources.length; ++i) {
      const shader = this.loadShader(shaderSources[i], types[i]);
      if (shader) shaders.push(shader);
    }
    return this.createProgram(shaders, optAttribs, optLocations);
  }

  getOffScreenContext() {
    return this.offScreenCanvas.getContext('webgl2');
  }

  public updateCanvas(viewer: OpenSeadragon.Viewer) {
    const viewportZoom = viewer.viewport.getZoom(true);
    //Don't update min/Max zoom if viewer is "frozen"
    // @ts-ignore
    if (viewer.panHorizontal) {
      this.viewportMinZoom = viewer.viewport.getMinZoom();
      this.viewportMaxZoom = viewer.viewport.getMaxZoom();
    }

    const offScreenContext = this.getOffScreenContext();
    if (!offScreenContext) return;
    for (let i = 0, count = viewer.world.getItemCount(); i < count; i++) {
      let image = viewer.world.getItemAt(i);
      if (image) {
        const zoom = image.viewportToImageZoom(viewportZoom);
        const vp = image.imageToViewportCoordinates(0, 0, true);
        const p = viewer.viewport.pixelFromPoint(vp, true);
        this.translation = [p.x, p.y];
        this.scale = [zoom, zoom];

        let zoomLevel = (viewportZoom - this.viewportMinZoom) / (this.viewportMaxZoom - this.viewportMinZoom);
        zoomLevel = Math.log(this.zoomSpeed * zoomLevel + 1) / Math.log(this.zoomSpeed + 1);
        this.drawScene((this.polygonFactorMinZoom - this.polygonFactorMaxZoom) * zoomLevel + this.polygonFactorMaxZoom);

        // @ts-ignore
        let onScreen2dContext = viewer.canvas.firstChild.getContext('2d');
        if (!onScreen2dContext) {
          console.error("Can't get onScreen context");
          return;
        }

        onScreen2dContext.drawImage(
          offScreenContext.canvas,
          0,
          0,
          offScreenContext.canvas.width,
          offScreenContext.canvas.height,
          0,
          0,
          onScreen2dContext.canvas.width,
          onScreen2dContext.canvas.height,
        );
      }
    }
  }

  public static generateTriangle(size: number, angle: number): number[][] {
    const p1 = new Point((size / 2.0) * Math.cos(angle - HALF_PI), (size / 2.0) * Math.sin(angle - HALF_PI));
    const p2 = new Point((size / 2.0) * Math.cos(angle + 5 * SIXTH_PI), (size / 2.0) * Math.sin(angle + 5 * SIXTH_PI));
    const p3 = new Point((size / 2.0) * Math.cos(angle + SIXTH_PI), (size / 2.0) * Math.sin(angle + SIXTH_PI));
    return [
      [p1.x, p1.y],
      [p2.x, p2.y],
      [p3.x, p3.y],
    ];
  }

  public static generateSquare(size: number, angle: number): number[][] {
    const p1 = new Point((size / 2.0) * Math.cos(angle + FOURTH_PI), (size / 2.0) * Math.sin(angle + FOURTH_PI));
    const p2 = new Point(
      (size / 2.0) * Math.cos(angle + 3 * FOURTH_PI),
      (size / 2.0) * Math.sin(angle + 3 * FOURTH_PI),
    );
    const p3 = new Point(
      (size / 2.0) * Math.cos(angle - 3 * FOURTH_PI),
      (size / 2.0) * Math.sin(angle - 3 * FOURTH_PI),
    );
    const p4 = new Point((size / 2.0) * Math.cos(angle - FOURTH_PI), (size / 2.0) * Math.sin(angle - FOURTH_PI));
    return [
      [p1.x, p1.y],
      [p2.x, p2.y],
      [p3.x, p3.y],
      [p4.x, p4.y],
    ];
  }

  public static generateDiamond(size: number, angle: number): number[][] {
    const p1 = new Point((size / 4.0) * Math.cos(angle), (size / 4.0) * Math.sin(angle));
    const p2 = new Point((size / 2.0) * Math.cos(angle + HALF_PI), (size / 2.0) * Math.sin(angle + HALF_PI));
    const p3 = new Point((size / 4.0) * Math.cos(angle + Math.PI), (size / 4.0) * Math.sin(angle + Math.PI));
    const p4 = new Point((size / 2.0) * Math.cos(angle - HALF_PI), (size / 2.0) * Math.sin(angle - HALF_PI));
    return [
      [p1.x, p1.y],
      [p2.x, p2.y],
      [p3.x, p3.y],
      [p4.x, p4.y],
    ];
  }

  public static generateCross(size: number): number[][] {
    const thickness: number = size / 8.0;
    const p1 = new Point(size / 2.0, thickness);
    const p2 = new Point(thickness, thickness);
    const p3 = new Point(thickness, size / 2.0);
    const p4 = new Point(-thickness, size / 2.0);
    const p5 = new Point(-thickness, thickness);
    const p6 = new Point(-size / 2.0, thickness);
    const p7 = new Point(-size / 2.0, -thickness);
    const p8 = new Point(-thickness, -thickness);
    const p9 = new Point(-thickness, -size / 2.0);
    const p10 = new Point(thickness, -size / 2.0);
    const p11 = new Point(thickness, -thickness);
    const p12 = new Point(size / 2.0, -thickness);
    return [
      [p1.x, p1.y],
      [p2.x, p2.y],
      [p3.x, p3.y],
      [p4.x, p4.y],
      [p5.x, p5.y],
      [p6.x, p6.y],
      [p7.x, p7.y],
      [p8.x, p8.y],
      [p9.x, p9.y],
      [p10.x, p10.y],
      [p11.x, p11.y],
      [p12.x, p12.y],
    ];
  }

  public static generateWye(size: number, angle: number): number[][] {
    const innerrad = size / 4.0;
    const EIGHTH_PI = Math.PI / 10.0;
    const p1 = new Point(innerrad * Math.cos(angle - HALF_PI), innerrad * Math.sin(angle + HALF_PI));
    const p2 = new Point(
      (size / 2.0) * Math.cos(angle + Math.PI - SIXTH_PI - EIGHTH_PI),
      (size / 2.0) * Math.sin(angle + Math.PI - SIXTH_PI - EIGHTH_PI),
    );
    const p3 = new Point(
      (size / 2.0) * Math.cos(angle + Math.PI - SIXTH_PI + EIGHTH_PI),
      (size / 2.0) * Math.sin(angle + Math.PI - SIXTH_PI + EIGHTH_PI),
    );
    const p4 = new Point(innerrad * Math.cos(angle - 5 * SIXTH_PI), innerrad * Math.sin(angle - 5 * SIXTH_PI));
    const p5 = new Point(
      (size / 2.0) * Math.cos(angle - HALF_PI - EIGHTH_PI),
      (size / 2.0) * Math.sin(angle - HALF_PI - EIGHTH_PI),
    );
    const p6 = new Point(
      (size / 2.0) * Math.cos(angle - HALF_PI + EIGHTH_PI),
      (size / 2.0) * Math.sin(angle - HALF_PI + EIGHTH_PI),
    );
    const p7 = new Point(innerrad * Math.cos(angle - SIXTH_PI), innerrad * Math.sin(angle - SIXTH_PI));
    const p8 = new Point(
      (size / 2.0) * Math.cos(angle + SIXTH_PI - EIGHTH_PI),
      (size / 2.0) * Math.sin(angle + SIXTH_PI - EIGHTH_PI),
    );
    const p9 = new Point(
      (size / 2.0) * Math.cos(angle + SIXTH_PI + EIGHTH_PI),
      (size / 2.0) * Math.sin(angle + SIXTH_PI + EIGHTH_PI),
    );
    return [
      [p1.x, p1.y],
      [p2.x, p2.y],
      [p3.x, p3.y],
      [p4.x, p4.y],
      [p5.x, p5.y],
      [p6.x, p6.y],
      [p7.x, p7.y],
      [p8.x, p8.y],
      [p9.x, p9.y],
    ];
  }
}
