import { Viewer } from 'openseadragon';
import { WildOsd } from './WildOsd';

export class FixedOverlay {
  private viewer: Viewer;
  private osd: WildOsd;
  private containerWidth: number;
  private containerHeight: number;
  private readonly element: HTMLElement;

  constructor(osd: WildOsd, root: (Node & ParentNode) | null, element: HTMLElement) {
    this.viewer = osd.osdViewer;
    this.osd = osd;
    this.containerWidth = 0;
    this.containerHeight = 0;
    this.element = element;
    this.element.style.position = 'absolute';

    if (root) root.insertBefore(this.element, root.childNodes[0]); // Insert on top
    else this.viewer.canvas.appendChild(this.element);
  }

  public setViewportPosition(x: number, y: number) {
    if (this.osd.isMaster) {
      this.element.style.left = Math.round(x * this.osd.osdViewer.canvas.clientWidth) + 'px';
      this.element.style.top = Math.round(y * this.osd.osdViewer.canvas.clientHeight) + 'px';
    } else {
      this.element.style.left = Math.round(x * this.osd.wallWidth - this.osd.tileOffsetX) + 'px';
      this.element.style.top = Math.round(y * this.osd.wallHeight - this.osd.tileOffsetY) + 'px';
    }
  }

  public setViewportSize(x: number, y: number) {
    if (this.osd.isMaster) {
      this.element.style.width = Math.round(x * this.osd.osdViewer.canvas.clientWidth) + 'px';
      this.element.style.height = Math.round(y * this.osd.osdViewer.canvas.clientHeight) + 'px';
    } else {
      this.element.style.width = Math.round(x * this.osd.wallWidth) + 'px';
      this.element.style.height = Math.round(y * this.osd.wallHeight) + 'px';
    }
  }
}
