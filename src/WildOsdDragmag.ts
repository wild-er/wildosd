import OpenSeadragon, { EventHandler, MouseTrackerEvent, OSDEvent, Viewer } from 'openseadragon';
import { WildOsd } from './WildOsd';
import { WildOsdWindow } from './WildOsdWindow';
import { WildCanvasOverlay } from './WildCanvasOverlay';

export class WildOsdDragmag extends WildOsdWindow {
  readonly viewer: OpenSeadragon.Viewer;

  public pinchScaleRatio = 10;
  constructor(wildOsd: WildOsd, viewer: OpenSeadragon.Viewer, div: HTMLElement) {
    super(wildOsd, div);
    this.viewer = viewer;
    this.mouseTracker.element = viewer.canvas;
  }

  public zoomMode() {
    this.mouseTracker.setTracking(false);
    this.viewer.setMouseNavEnabled(true);
  }

  public resizeMode() {
    this.mouseTracker.setTracking(true);
    this.viewer.setMouseNavEnabled(false);
  }

  public destroy() {
    this.viewer.destroy();
    this.div.remove();
  }

  public AddOverlay(overlay: WildCanvasOverlay) {
    this.div.style.position = 'absolute';
    this.viewer.addHandler('update-viewport', () => {
      overlay.resize(this.viewer);
      overlay.updateCanvas(this.viewer);
    });
  }

  public fitBounds(rect: OpenSeadragon.Rect) {
    this.viewer.viewport.fitBounds(rect, true);
  }
}
