import { WildOsd } from './WildOsd';
import { WildCanvasOverlay } from './WildCanvasOverlay';
import OpenSeadragon, { MouseTrackerEvent } from 'openseadragon';

export class WildOsdWindow {
  protected wildOsd: WildOsd;
  readonly div: HTMLElement;
  protected winDelta: OpenSeadragon.Point;
  protected isDragging = false;
  public isLocked = false;
  public windowSizeMin = 100;
  protected mouseTracker: OpenSeadragon.MouseTracker;
	protected onDestroy?: ()=>void;

  constructor(wildOsd: WildOsd, div: HTMLElement, onDestroy?:()=>void) {
    this.wildOsd = wildOsd;
    this.div = div;
		if (onDestroy)
			this.onDestroy=onDestroy;
    this.winDelta = new OpenSeadragon.Point(0, 0);
    this.mouseTracker = new OpenSeadragon.MouseTracker({
      element: div,
      preProcessEventHandler: (event) => {
        // @ts-ignore
        event.stopPropagation = true;
        // event.preventGesture = false;
        //event.defaultPrevented = true;
        event.preventDefault = true;
      },
      clickTimeThreshold: 300,
      clickDistThreshold: 50,
      dblClickTimeThreshold: 300,
      dblClickDistThreshold: 50,
      pinchHandler: (event: MouseTrackerEvent<Event>) => {
        // @ts-ignore
        let width = parseInt(this.div.style.width) + (event.distance - event.lastDistance) * this.pinchScaleRatio;
        // @ts-ignore
        let height = parseInt(this.div.style.height) + (event.distance - event.lastDistance) * this.pinchScaleRatio;
        if (width >= this.windowSizeMin && height >= this.windowSizeMin) {
          this.wildOsd.windowSetTransform(
            this.div.id,
            parseInt(this.div.style.left),
            parseInt(this.div.style.top),
            // @ts-ignore
            width,
            // @ts-ignore
            height,
          );
        }
      },
      scrollHandler: (event: MouseTrackerEvent<Event>) => {
        // @ts-ignore
        let scrollValue = parseInt(event.scroll);
        this.wildOsd.windowSetTransform(
          this.div.id,
          parseInt(this.div.style.left) - scrollValue,
          parseInt(this.div.style.top) - scrollValue,
          parseInt(this.div.style.width) + scrollValue * 2,
          parseInt(this.div.style.height) + scrollValue * 2,
        );
      },
      dragHandler: (event: MouseTrackerEvent<Event>) => {
        if (!this.isDragging) {
          this.isDragging = true;
          this.winDelta = new OpenSeadragon.Point(parseInt(this.div.style.left), parseInt(this.div.style.top));
        } else {
          // @ts-ignore
          let delta = event.delta;
          this.winDelta.x += delta.x;
          this.winDelta.y += delta.y;
          this.wildOsd.windowSetTransform(
            this.div.id,
            this.winDelta.x,
            this.winDelta.y,
            parseInt(this.div.style.width),
            parseInt(this.div.style.height),
          );
        }
      },
      dragEndHandler: (event) => {
        this.isDragging = false;
      },
    });
  }

  public zoomMode() {}

  public resizeMode() {}

  public destroy() {
		if (this.onDestroy)
			this.onDestroy();
    this.div.remove();
  }

  public AddOverlay(overlay: WildCanvasOverlay) {}

  public fitBounds(rect: OpenSeadragon.Rect) {}

  public onSetTransform(x: number, y: number, w: number, h: number) {}
}
