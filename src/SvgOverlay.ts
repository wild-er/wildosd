// From https://github.com/openseadragon/svg-overlay/blob/master/openseadragon-svg-overlay.js
import { Viewer, Point, MouseTracker } from 'openseadragon';

const svgNS: string = 'http://www.w3.org/2000/svg';

export class SvgOverlay {
  private viewer: Viewer;
  private containerWidth: number;
  private containerHeight: number;
  private readonly svg: SVGElement;
  node: SVGGElement;

  constructor(viewer: OpenSeadragon.Viewer, root: (Node & ParentNode) | null, id: string) {
    this.viewer = viewer;
    this.containerWidth = 0;
    this.containerHeight = 0;
    this.svg = document.createElementNS(svgNS, 'svg') as SVGElement;
    this.svg.id = id;
    this.svg.style.position = 'absolute';
    this.svg.style.left = '0';
    this.svg.style.top = '0';
    this.svg.style.width = '100%';
    this.svg.style.height = '100%';
    if (root) root.insertBefore(this.svg, root.childNodes[0]); // Insert on top for now
    else viewer.canvas.appendChild(this.svg);
    this.node = document.createElementNS(svgNS, 'g') as SVGGElement;
    this.svg.appendChild(this.node);

    this.viewer.addHandler('animation', () => {
      this.resize();
    });

    this.viewer.addHandler('open', () => {
      this.resize();
    });

    this.viewer.addHandler('rotate', (evt) => {
      this.resize();
    });

    this.viewer.addHandler('resize', () => {
      this.resize();
    });
  }

  resize() {
    if (this.containerWidth !== this.viewer.container.clientWidth) {
      this.containerWidth = this.viewer.container.clientWidth;
      this.svg.setAttribute('width', `${this.containerWidth}`);
    }

    if (this.containerHeight !== this.viewer.container.clientHeight) {
      this.containerHeight = this.viewer.container.clientHeight;
      this.svg.setAttribute('height', `${this.containerHeight}`);
    }

    const p = this.viewer.viewport.pixelFromPoint(new Point(0, 0), true);
    const zoom = this.viewer.viewport.getZoom(true);
    const rotation = this.viewer.viewport.getRotation();
    // TODO: Expose an accessor for _containerInnerSize in the OSD API so we don't have to use the private variable.
    // @ts-ignore
    const scale = this.viewer.viewport._containerInnerSize.x * zoom;
    this.node.setAttribute(
      'transform',
      'translate(' + p.x + ',' + p.y + ') scale(' + scale + ') rotate(' + rotation + ')',
    );
  }

  onClick(node: string, handler: OpenSeadragon.EventHandler<OpenSeadragon.OSDEvent<any>> | undefined) {
    new MouseTracker({
      element: node,
      clickHandler: handler,
    }).setTracking(true);
  }
}
