import OpenSeadragon from 'openseadragon';
export abstract class WildCanvasOverlay {
  protected viewer;
  protected backingScale = 1;
  protected containerWidth = 0;
  protected containerHeight = 0;
  protected offScreenCanvas: HTMLCanvasElement;

  constructor(viewer: OpenSeadragon.Viewer, options: OpenSeadragon.Options) {
    this.viewer = viewer;

    // this.viewer.addHandler('update-viewport', () => {
    //   this.resize(this.viewer);
    //   this.updateCanvas(this.viewer);
    // });
    // this.viewer.addHandler('open', () => {
    //   this.resize(this.viewer);
    //   this.updateCanvas(this.viewer);
    // });

    this.offScreenCanvas = document.createElement('canvas');
  }

  public abstract updateCanvas(viewer: OpenSeadragon.Viewer): void;

  public abstract resize(viewer: OpenSeadragon.Viewer): void;
}
