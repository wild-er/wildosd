import OpenSeadragon, { OSDEvent, Placement, Point, Rect, Viewer, ViewerEvent } from 'openseadragon';
import io from 'socket.io-client';
import { WildOsdDragmag } from './WildOsdDragmag';
import { WildCanvasOverlay } from './WildCanvasOverlay';
import { WildOsdWindow } from './WildOsdWindow';

export interface WildOsdViewerOptions {
  osdPyramidLevel?: number;
  osdTileSize?: number;
  osdTileUrl?: (z: number, x: number, y: number) => string;
	osdTileSource?: string;
}

export interface WildOsdOptions extends WildOsdViewerOptions {
  id: string;
  useWebGl?: boolean;
  syncServerIp: string;
  syncServerPort: number;
  wallWidth: number;
  wallHeight: number;
  tileWidth: number;
  tileHeight: number;
  tileOffsetX: number;
  tileOffsetY: number;
  tuioServerIp: string;
  tuioServerPort: number;
  selectionEnd?: (imageTopLeft: Point, y: Point) => void;
  clickDistThreshold: number;
  dblClickDistThreshold: number;
}

export interface Rpc {
  func: string;
  args?: any[];
}

export interface WildOsdWallRect {
	x: number;
	y: number;
	w: number;
	h: number;
}

interface TuioMessage {
  xPos: number;
  yPos: number;
  cursorId: number;
}

interface pointerInfo {
  cursorId: number;
  pointedElement: Element;
}

interface Selection {
  div: HTMLDivElement;
  color: string;
}
export class WildOsd {
  // Osd
  private minZoomLevel: number;
  private maxZoomLevel: number;
  public osdViewer: OpenSeadragon.Viewer;
  private element: HTMLElement;
	public overlayRootDiv:HTMLDivElement;

  readonly wallWidth: number;
  readonly wallHeight: number;
  readonly tileWidth: number;
  readonly tileHeight: number;
  readonly tileOffsetX: number;
  readonly tileOffsetY: number;

  // Network sync
  private id: string;
  public readonly isMaster: boolean;
  private socketViewSync: SocketIOClient.Socket;

  // Selection
  public isSelectionMode: boolean = false;
  private selectionStartingPoint: Point | undefined = undefined;
  private isSelecting: boolean = false;
  public readonly SELECTION_ID = 'selection';
  private selectionEnd?: (imageTopLeft: Point, y: Point) => void;
  private selectionElements: Map<string, Selection>;

  // Touch input
  private socketInput: SocketIOClient.Socket | null = null;
  private pointerInfos: Map<number, Element>;
	private pointersHistory: Map<number, TuioMessage>;

  // Drag Mag / Windows
  private windows: Map<string, WildOsdWindow>;
  public windowOutlineThickness = 20;
  private windowBorderTouched: Map<number, string>;

  constructor(id: string, element: HTMLElement, options: WildOsdOptions) {
    // OSD
    this.osdViewer = this.createOsdViewer(id, element, options);

		this.overlayRootDiv= this.osdViewer.canvas.children[1] as HTMLDivElement; //Created by osd

		this.overlayRootDiv.style.height="100%"; //Bug fix...

		// @ts-ignore
    this.minZoomLevel = this.osdViewer.viewport.minZoomLevel;
    // @ts-ignore
    this.maxZoomLevel = this.osdViewer.viewport.maxZoomLevel;

    this.element = element;

    // Sync
    this.id = options.id;
    this.socketViewSync = io(`http://${options.syncServerIp}:${options.syncServerPort}`);

    this.socketViewSync.emit('ID', this.id);
    if (this.id === 'MASTER') {
      this.isMaster = true;
      this.osdViewer.addHandler('viewport-change', () => {
        //Send new position/zoom to server
        const rect: OpenSeadragon.Rect = this.osdViewer.viewport.getBounds();
        this.rpCall('viewportTransform', [rect.x, rect.y, rect.width, rect.height]);
      });
    } else {
      this.isMaster = false;
    }

    this.socketViewSync.on('Rpc', (funcToCall: Rpc) => {
      const code: any = funcToCall.func;
      // @ts-ignore
      window[code].apply(this, funcToCall.args);
    });

    this.wallWidth = options.wallWidth;
    this.wallHeight = options.wallHeight;

    this.tileWidth = options.tileWidth;
    this.tileHeight = options.tileHeight;

    this.tileOffsetX = options.tileOffsetX;
    this.tileOffsetY = options.tileOffsetY;

    //Selection
		if (options.selectionEnd)
    	this.selectionEnd = options.selectionEnd;
    this.selectionElements = new Map<string, Selection>();

    this.osdViewer.addHandler('canvas-drag', (viewerEvent) => {
      if (this.isSelectionMode && !this.isSelecting && viewerEvent.position) {
        this.selectionStartingPoint = viewerEvent.position;
        this.isSelecting = true;
      }

      if (this.isSelectionMode && viewerEvent.position && this.selectionStartingPoint) {
        const elementRect = new Rect(
          Math.min(this.selectionStartingPoint.x, viewerEvent.position.x),
          Math.min(this.selectionStartingPoint.y, viewerEvent.position.y),
          Math.abs(this.selectionStartingPoint.x - viewerEvent.position.x),
          Math.abs(this.selectionStartingPoint.y - viewerEvent.position.y),
        );

        const viewportRect = this.osdViewer.viewport.viewerElementToViewportRectangle(elementRect);
        const imageRect = this.osdViewer.viewport.viewportToImageRectangle(
          viewportRect.x,
          viewportRect.y,
          viewportRect.width,
          viewportRect.height,
        );

        // Display/update rectangle locally
        this.selectionTransform(this.SELECTION_ID, 'red', imageRect.x, imageRect.y, imageRect.width, imageRect.height);
        // and remotely
        this.rpCall('selectionTransform', [
          this.SELECTION_ID,
          'red',
          imageRect.x,
          imageRect.y,
          imageRect.width,
          imageRect.height,
        ]);
      }
    });

    this.osdViewer.addHandler('canvas-drag-end', (viewerEvent) => {
      if (viewerEvent.position && this.selectionStartingPoint && this.isSelecting) {
        console.log('drag end -->' + viewerEvent.position.x + ' ' + viewerEvent.position.y);

        const left = Math.min(viewerEvent.position.x, this.selectionStartingPoint.x);
        const right = Math.max(viewerEvent.position.x, this.selectionStartingPoint.x);
        const top = Math.min(viewerEvent.position.y, this.selectionStartingPoint.y);
        const bottom = Math.max(viewerEvent.position.y, this.selectionStartingPoint.y);

        const imageTopLeft = this.osdViewer.viewport.viewerElementToImageCoordinates(new Point(left, top));
        const imageBottomRight = this.osdViewer.viewport.viewerElementToImageCoordinates(new Point(right, bottom));

        // Callback
				if (this.selectionEnd)
        	this.selectionEnd(imageTopLeft, imageBottomRight);
      }
      this.isSelecting = false;
    });

    // Touch input
    this.pointerInfos = new Map<number, Element>();
		this.pointersHistory = new Map<number,TuioMessage>();

    if (this.isMaster) {
      this.socketInput = io('http://' + options.tuioServerIp + ':' + options.tuioServerPort);

      this.socketInput.on('onAdd', (json: TuioMessage) => {
        let roundX = Number(json.xPos.toFixed(3));
        let roundY = Number(json.yPos.toFixed(3));

        //console.log('onAdd ' +json.cursorId + ' ' + json.xPos + ' '+ json.yPos +' -> '+roundX+' '+roundY);

				this.pointersHistory.set(json.cursorId,json);
        if (roundX == 0.0 && roundY == 0.0) console.log('false add touch');
        else this.LaunchTouchEvent(json, 'pointerdown');
      });

      this.socketInput.on('onUpdate', (json: TuioMessage) => {
        let roundX = Number(json.xPos.toFixed(3));
        let roundY = Number(json.yPos.toFixed(3));

        //console.log('onUpdate '+ json.xPos + ' '+ json.yPos );

        if (roundX == 0.0 && roundY == 0.0) console.log('false add touch');
        else this.LaunchTouchEvent(json, 'pointermove');
      });

      this.socketInput.on('onRemove', (json: TuioMessage) => {
        let roundX = Number(json.xPos.toFixed(3));
        let roundY = Number(json.yPos.toFixed(3));

        //console.log('onRemove '+ json.xPos + ' '+ json.yPos +' -> '+roundX+' '+roundY);

				if (roundX == 0.0 && roundY == 0.0) {
					if (!this.pointersHistory.get(json.cursorId)) {
						console.log('false remove touch');
						return;
					}
				}
        else this.LaunchTouchEvent(json, 'pointerup');
				this.pointersHistory.delete(json.cursorId);
			});
    }

    // Window
    this.windows = new Map<string, WildOsdWindow>();
    this.windowBorderTouched = new Map<number, string>();
    if (this.isMaster) {
      this.osdViewer.addHandler('canvas-press', (viewerEvent: OSDEvent<Viewer>) => {
        // @ts-ignore
        let windowElementId = this._isWindowOutlineClicked(viewerEvent.position);
        if (windowElementId) {
          // @ts-ignore
          this.windowBorderTouched.set(viewerEvent.originalEvent.pointerId, windowElementId);
          let window = this.windows.get(windowElementId)!;
          if (window.isLocked) {
            //unselect
            this.windowHighlight(windowElementId, false);
            window.resizeMode();
            window.isLocked = false;
          } else {
            this.windowHighlight(windowElementId, true);
            window.zoomMode();
            window.isLocked = true;
          }
        }
      });
    }

    // RPC Make functions visible to be remotely called
    this.rpRegister('viewportTransform', this.viewportTransform);
    this.rpRegister('selectionTransform', this.selectionTransform);
    this.rpRegister('overlayRemove', this.overlayRemove);
    this.rpRegister('windowSetTransform', this.windowSetTransform);
    this.rpRegister('windowHighlight', this.windowHighlight);
    this.rpRegister('addDragMagOsd', this.addDragMagOsd);
    this.rpRegister('addWindow', this.addWindow);
    this.rpRegister('removeWindowById', this.removeWindowById);
  }

  private createOsdViewer(id: string, element: HTMLElement, options: WildOsdViewerOptions): OpenSeadragon.Viewer {
		let tilesource;
    if (options.osdTileSource) {
				tilesource = options.osdTileSource;
		} else if (options.osdTileSize && options.osdPyramidLevel && options.osdTileUrl){
			const tilespyramidWidth = options.osdTileSize * Math.pow(2, options.osdPyramidLevel);
			const tilePyramidHeight = options.osdTileSize * Math.pow(2, options.osdPyramidLevel);
			tilesource = {
				height: tilespyramidWidth,
				width: tilePyramidHeight,
				tileSize: options.osdTileSize,
				minLevel: 0,
				maxLevel: options.osdPyramidLevel,
				tileOverlap: 0,
				getTileUrl: options.osdTileUrl,
			}
		} else {
			console.error('Not enough parameter to create osd viewer');
		}

		// total width & height of tile pyramid

    let viewer = OpenSeadragon({
      element: element,
      tileSources: tilesource,
      gestureSettingsTouch: {
        dblClickToZoom: false,
        clickToZoom: false,
      },
      gestureSettingsMouse: {
        dblClickToZoom: false,
        clickToZoom: false,
      },
      showNavigator: false,
      blendTime: 0,
      showNavigationControl: false,
      visibilityRatio: 0,
      wrapHorizontal: false,
      debugMode: false,
      animationTime: 0,
			crossOriginPolicy:"Anonymous",
      // @ts-ignore
      subPixelRoundingForTransparency: OpenSeadragon.SUBPIXEL_ROUNDING_OCCURRENCES.ALWAYS
    });

    viewer.element.id = id;

    return viewer;
  }

  // master broadcasts viewport movements through server with this rpcall
  public rpCall(func: string, args: any[], socket?: SocketIOClient.Socket) {
    if (typeof socket == 'undefined') {
      socket = this.socketViewSync;
    }
    socket.emit('Rpc', { func: func, args: args });
  }

  public rpRegister(id: string, rp: any) {
    // @ts-ignore
    window[id] = rp;
  }

  public viewportTransform(x: number, y: number, width: number, height: number, viewerId?: string) {
    const zoomedWidth: number = width * (this.tileWidth / this.wallWidth);
    const zoomedHeight: number = height * (this.tileHeight / this.wallHeight);
    const zoomedOffsetX: number = x + (this.tileOffsetX / this.wallWidth) * width;
    const zoomedOffsetY: number = y + (this.tileOffsetY / this.wallHeight) * height;

    if (viewerId == undefined)
      this.osdViewer.viewport.fitBounds(
        new OpenSeadragon.Rect(zoomedOffsetX, zoomedOffsetY, zoomedWidth, zoomedHeight),
        true,
      );
    else {
      this.windows.get(viewerId)!.fitBounds(new OpenSeadragon.Rect(x, y, width, height));
    }
  }

  // SELECTION
  public selectionTransform(id: string, color: string, x: number, y: number, width: number, height: number) {
    const viewportRect = this.osdViewer.viewport.imageToViewportRectangle(x, y, width, height);

    let elementAlreadyCreated = false;
    let element = this.selectionElements.get(id);
    if (element) {
      const selectionOverlay: OpenSeadragon.Overlay = this.osdViewer.getOverlayById(element.div);
      if (selectionOverlay) {
        selectionOverlay.update(viewportRect, Placement.TOP_LEFT);
        elementAlreadyCreated = true;
      }
    }

    if (!elementAlreadyCreated) {
      const selectionElement = document.createElement('div');
      selectionElement.className = id;
      selectionElement.id = id;
      if (this.isMaster) selectionElement.style.outline = '2px solid';
      else selectionElement.style.outline = '10px solid';
      selectionElement.style.outlineColor = color;
      selectionElement.style.backgroundColor = 'transparent';
      selectionElement.style.pointerEvents = 'none';
      this.selectionElements.set(id, { div: selectionElement, color: color });
      this.osdViewer.addOverlay(selectionElement, viewportRect, Placement.TOP_LEFT);
    }

    this.osdViewer.forceRedraw();
  }

  public overlayRemove(id: string) {
    const overlay: OpenSeadragon.Overlay = this.osdViewer.getOverlayById(id);
    if (overlay) {
      this.osdViewer.removeOverlay(id);
    }
  }

  public createEventDict(json: TuioMessage): PointerEventInit {
    const screenX = screen.width * json.xPos;
    const screenY = screen.height * json.yPos;
    const clientX =
      (this.osdViewer.element.getBoundingClientRect().right - this.osdViewer.element.getBoundingClientRect().left) *
      json.xPos;
    const clientY =
      (this.osdViewer.element.getBoundingClientRect().bottom - this.osdViewer.element.getBoundingClientRect().top) *
      json.yPos;

    return {
      pointerId: json.cursorId + 1, //because 0 seems to be an invalid id as not written anywhere in docs ಠ_ಠ
      clientX: clientX,
      clientY: clientY,
      screenX: screenX,
      screenY: screenY,
      pointerType: 'touch',
      bubbles: true,
      cancelable: true,
      composed: true,
      isPrimary: json.cursorId === 0,
    };
  }

  public LaunchTouchEvent(json: TuioMessage, event: string) {
    const pointerEventInit: PointerEventInit = this.createEventDict(json);

    const pointerEvent = new PointerEvent(event, pointerEventInit);
    let pointedElement = document.elementFromPoint(
      pointerEventInit.clientX as number,
      pointerEventInit.clientY as number,
    );
    if (!pointedElement) {
      pointedElement = this.osdViewer.canvas;
      console.log('No pointed element ?');
    }

    if (event == 'pointerdown') {
      const pointerEnterEvent = new PointerEvent('pointerenter', pointerEventInit);
      pointedElement.dispatchEvent(pointerEnterEvent);
      this.pointerInfos.set(json.cursorId, pointedElement);
      pointedElement.dispatchEvent(pointerEvent);
      return;
    }

    if (event == 'pointermove') {
      let previousPointedElement = this.pointerInfos.get(json.cursorId);
      //if(pointedElement != previousPointedElement) {
      if (previousPointedElement) {
        // 		const pointerOutEvent = new PointerEvent('pointerout', pointerEventInit);
        // 		previousPointedElement.dispatchEvent(pointerOutEvent);
        // 	}
        previousPointedElement.dispatchEvent(pointerEvent);
      }
    }

    if (event == 'pointerup') {
      //const pointerEnterEvent = new PointerEvent('pointercancel', pointerEventInit);
      let previousPointedElement = this.pointerInfos.get(json.cursorId);
      if (previousPointedElement) {
        previousPointedElement.dispatchEvent(pointerEvent);
      }
      this.pointerInfos.delete(json.cursorId);
    }
  }

	//Horrible hack to try to fix app when the touch frame "forget" to send a release event...
	public resetTouch() {
		for (const pointerInfos of this.pointersHistory.values()) {
			this.LaunchTouchEvent(pointerInfos,'pointerup');
		}
	}

  // Selection
  public freezeOSD() {
    // Freeze pan & zoom
    // @ts-ignore
    this.osdViewer.panHorizontal = false;
    // @ts-ignore
    this.osdViewer.panVertical = false;
    const zoom = this.osdViewer.viewport.getZoom();
    // @ts-ignore
    this.osdViewer.viewport.minZoomLevel = this.osdViewer.viewport.maxZoomLevel = zoom;
  }

  public startSelectionMode() {
    if (!this.isSelectionMode) {
      this.isSelectionMode = true;
      this.freezeOSD();
    }
  }

  public unfreezeOSD() {
    // Unfreeze pan & zoom
    // @ts-ignore
    this.osdViewer.panHorizontal = true;
    // @ts-ignore
    this.osdViewer.panVertical = true;
    // @ts-ignore
    this.osdViewer.viewport.minZoomLevel = this.minZoomLevel;
    // @ts-ignore
    this.osdViewer.viewport.maxZoomLevel = this.maxZoomLevel;
  }

  public endSelectionMode() {
    if (this.isSelectionMode) {
      this.isSelectionMode = false;
      this.unfreezeOSD();
    }
  }

  // Window
  private createWindow(
    elementId: string,
    id: string,
    color: string,
    x: number,
    y: number,
    width: number,
    height: number
	): HTMLDivElement | null {
    let windowDiv: HTMLDivElement;
    windowDiv = document.createElement('div');
    let element = document.getElementById(elementId);
    if (!element) {
      console.error("Can't find root element id " + elementId);
      return null;
    }
    element.insertAdjacentElement('beforebegin', windowDiv);
    windowDiv.style.left = String(x) + 'px';
    windowDiv.style.top = String(y) + 'px';
    windowDiv.style.width = width + 'px';
    windowDiv.style.height = height + 'px';
    windowDiv.id = id;
    windowDiv.style.position = 'absolute'; // call after viewer
    windowDiv.style.outline = 'solid ' + color + ' ' + this.windowOutlineThickness + 'px';
    windowDiv.style.userSelect = 'none';

    return windowDiv;
  }

  public addWindow(
    elementId: string,
    id: string,
    color: string,
    x: number,
    y: number,
    width: number,
    height: number,
		onDestroy?:()=>void
  ): HTMLDivElement | null {
    let windowDiv = this.createWindow(elementId, id, color, x, y, width, height);

    if (windowDiv) this.windows.set(id, new WildOsdWindow(this, windowDiv,onDestroy));
    return windowDiv;
  }

  public addDragMagOsd(
    rootElementId: string,
    id: string,
    color: string,
    options: WildOsdViewerOptions,
    x: number,
    y: number,
    width: number,
    height: number,
  ): OpenSeadragon.Viewer | null {
    let dragMagDiv = this.addWindow(rootElementId, id, color, x, y, width, height);
    if (!dragMagDiv) return null;

    let viewer = this.createOsdViewer(id, dragMagDiv, options);
    viewer.canvas.style.userSelect = 'none';

    this.windows.set(id, new WildOsdDragmag(this, viewer, dragMagDiv));

    if (this.isMaster) {
      viewer.addHandler('viewport-change', () => {
        //Send new position/zoom to server
        const rect: OpenSeadragon.Rect = viewer.viewport.getBounds();
        this.rpCall('viewportTransform', [rect.x, rect.y, rect.width, rect.height, id]);

        const imageRect = viewer.viewport.viewportToImageRectangle(rect.x, rect.y, rect.width, rect.height);
        //Change on master
        this.selectionTransform(id, color, imageRect.x, imageRect.y, imageRect.width, imageRect.height);
        // and on the wall
        this.rpCall('selectionTransform', [id, color, imageRect.x, imageRect.y, imageRect.width, imageRect.height]);
      });

      //Bug fix
      this.osdViewer.setMouseNavEnabled(false);
      this.osdViewer.setMouseNavEnabled(true);
    }

    viewer.setMouseNavEnabled(false);

    return viewer;
  }

  public removeWindowById(id: string) {
    let window = this.windows.get(id);
    if (window) {
      window.destroy();
      this.windows.delete(id);
      if (this.isMaster) this.rpCall('removeWindowById', [id]);
    }
  }

  public removeWindowByPosition(x: number, y: number): string {
    let windowId = this.isWindowOutlineClicked(x, y);
    if (windowId) this.removeWindowById(windowId);
    return windowId;
  }

  public windowHighlight(id: string, isHighlighted: boolean) {
    let windowDiv = this.windows.get(id)!.div;
    if (isHighlighted) windowDiv.style.outlineStyle = 'dashed';
    else windowDiv.style.outlineStyle = 'solid';

    if (this.isMaster) this.rpCall('windowHighlight', [id, isHighlighted]);
  }

  public getWindow(id: string): WildOsdWindow | undefined {
    return this.windows.get(id);
  }

  public windowSetTransform(id: string, x: number, y: number, w: number, h: number) {
    let window = this.windows.get(id);
    let windowDiv = window!.div;
    if (!windowDiv) {
      console.error(`Window ${id} not found`);
      return;
    }

    if (this.isMaster) {
      windowDiv.style.left = String(x) + 'px';
      windowDiv.style.top = String(y) + 'px';
      windowDiv.style.width = String(w) + 'px';
      windowDiv.style.height = String(h) + 'px';

			let rect = this.getWallPosition(x,y,w,h);

      this.rpCall('windowSetTransform', [id, rect.x, rect.y, rect.w, rect.h]);
    } else {
      windowDiv.style.left = String(x - this.tileOffsetX) + 'px';
      windowDiv.style.top = String(y - this.tileOffsetY) + 'px';
      windowDiv.style.width = String(w) + 'px';
      windowDiv.style.height = String(h) + 'px';
    }

    window!.onSetTransform(x, y, w, h);
  }

  public dragMagAddOverlay(id: string, overlay: WildCanvasOverlay) {
    let window = this.windows.get(id);
		if (window) {
			window.AddOverlay(overlay);
			window.div.style.position = 'absolute';
		}
	}

	public getWallPosition(x:number,y:number,w:number,h:number):WildOsdWallRect {
		return {
			x:(x * this.wallWidth) / this.tileWidth,
			y:(y * this.wallHeight) / this.tileHeight,
			w:(w * this.wallWidth) / this.tileWidth,
			h:(h * this.wallHeight) / this.tileHeight
		};
	}

  private isWindowOutlineClicked(x: number, y: number): string {
    for (const windowElementId of this.windows.keys()) {
      const windowElement = this.windows.get(windowElementId)!.div;
      const left = parseInt(windowElement.style.left);
      const right = left + parseInt(windowElement.style.width);
      const top = parseInt(windowElement.style.top);
      const bottom = top + parseInt(windowElement.style.height);

      if (
        x > left - this.windowOutlineThickness &&
        x < right + this.windowOutlineThickness &&
        y > top - this.windowOutlineThickness &&
        y < bottom + this.windowOutlineThickness
      ) {
        //we don't receive the click if inside the window so no need to substract the window itself
        return windowElementId;
      }
    }
    return '';
  }
  private _isWindowOutlineClicked(viewerEvent: ViewerEvent): string {
    // @ts-ignore
    return this.isWindowOutlineClicked(viewerEvent.x!, viewerEvent.y!);
  }

  public static isNwJs(): boolean {
    // @ts-ignore
    return typeof nw !== 'undefined';
  }

  public static setWindowPosition(x: number, y: number) {
    if (WildOsd.isNwJs()) {
      // @ts-ignore
      nw.Window.get().moveTo(x, y);
    }
  }
}
