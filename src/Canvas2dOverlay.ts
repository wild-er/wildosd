import OpenSeadragon from 'openseadragon';
import { WildCanvasOverlay } from './WildCanvasOverlay';

export class Canvas2dOverlay extends WildCanvasOverlay {
  public onRedraw;
  public clearBeforeRedraw;

  constructor(
    viewer: OpenSeadragon.Viewer,
    options: OpenSeadragon.Options,
    root: (Node & ParentNode) | null,
    onRedraw: (index: number, context: CanvasRenderingContext2D | null, x: number, y: number, zoom: number) => void,
    clearBeforeRedraw: boolean = true,
  ) {
    super(viewer, options);

    this.onRedraw = onRedraw;
    this.clearBeforeRedraw = clearBeforeRedraw;
  }

  getOffScreenContext() {
    return this.offScreenCanvas.getContext('2d');
  }

  clear() {
    // @ts-ignore
    this.offScreenCanvas
      .getContext('2d')
      .clearRect(0, 0, this.containerWidth * this.backingScale, this.containerHeight * this.backingScale);
  }

  public resize(viewer: OpenSeadragon.Viewer) {
    let backingScale = 1;
    if (typeof window !== 'undefined' && 'devicePixelRatio' in window) {
      backingScale = window.devicePixelRatio;
    }
    const backingScaleUpdated = this.backingScale !== backingScale;
    this.backingScale = backingScale;

    if (this.containerWidth !== viewer.container.clientWidth || backingScaleUpdated) {
      this.containerWidth = viewer.container.clientWidth;
      this.offScreenCanvas.width = this.containerWidth;
      console.log('resize width' + this.offScreenCanvas.width);
    }

    if (this.containerHeight !== viewer.container.clientHeight || backingScaleUpdated) {
      this.containerHeight = viewer.container.clientHeight;
      this.offScreenCanvas.height = this.containerHeight;
      console.log('resize height' + this.offScreenCanvas.height);
    }
  }

  public updateCanvas(viewer: OpenSeadragon.Viewer) {
    const viewportZoom = viewer.viewport.getZoom(true);
    if (this.clearBeforeRedraw) {
      this.clear();
    }
    const offScreenContext = this.getOffScreenContext();
    if (!offScreenContext) return;
    for (let i = 0, count = viewer.world.getItemCount(); i < count; i++) {
      const image = viewer.world.getItemAt(i);
      if (image) {
        const zoom = image.viewportToImageZoom(viewportZoom);
        const vp = image.imageToViewportCoordinates(0, 0, true);
        const p = viewer.viewport.pixelFromPoint(vp, true);
        offScreenContext.scale(this.backingScale, this.backingScale);
        offScreenContext.translate(p.x, p.y);
        offScreenContext.scale(zoom, zoom);
        this.onRedraw(i, offScreenContext, p.x, p.y, zoom);
        offScreenContext.setTransform(1, 0, 0, 1, 0, 0);

        // @ts-ignore
        let onScreen2dContext = viewer.canvas.firstChild.getContext('2d');
        if (!onScreen2dContext) {
          console.error("Can't get onScreen context");
          return;
        }

        // @ts-ignore
        onScreen2dContext.drawImage(this.offScreenCanvas, 0, 0);
      }
    }
  }
}
