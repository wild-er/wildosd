# WildOsd

OpenSeadragon TMS pyramids on ultra-high-resolution wall displays

Web-based map visualization on cluster-driven UHRWD using OpenSeaDragon (seadragon.github.io)

Web-based map visualization on cluster-driven UHRWD with support for 2D graphics overlay (SVG, WebGL, ...).

See wildmap-ais and wildmap-sat for examples of use.

## Installation

```npm install```

```npm run build```
